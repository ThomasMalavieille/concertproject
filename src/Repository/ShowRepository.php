<?php

namespace App\Repository;

use App\Entity\Show;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Show|null find($id, $lockMode = null, $lockVersion = null)
 * @method Show|null findOneBy(array $criteria, array $orderBy = null)
 * @method Show[]    findAll()
 * @method Show[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShowRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Show::class);
    }

    /**
     * @return Show[] Returns an array of Show objects
    */

    public function ComingShow()
    {
        return $this->createQueryBuilder('show')
            ->andWhere('show.date >= :now')
            ->setParameter('now', new \DateTime('now'))
            ->orderBy('show.date', 'ASC') // Du plus tôt au plus tard
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Show[] Returns an array of Show objects
    */

    public function ComingShowByBandId($id)
    {
        return $this->createQueryBuilder('show')
            ->andWhere('show.band = :id')
            ->andWhere('show.date >= :now')
            ->setParameter('now', new \DateTime('now'))
            ->setParameter('id', $id)
            ->orderBy('show.date', 'ASC') // Du plus tôt au plus tard
            ->getQuery()
            ->getResult()
        ;
    }


    /**
     * @return Show[] Returns an array of Show objects
    */

    public function PassedShow()
    {
        return $this->createQueryBuilder('show')
            ->andWhere('show.date < :now')
            ->setParameter('now', new \DateTime('now'))
            ->orderBy('show.date', 'DESC') //Du plus récent au plus ancien
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Show
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
