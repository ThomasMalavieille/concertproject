<?php

namespace App\Controller;

use App\Entity\Hall;
use App\Entity\ConcertHall;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/hall")
 */

class HallController extends AbstractController
{
    /**
     * @Route(name="hall")
     */
    public function index(): Response
    {
        $halls = $this->getDoctrine()->getRepository(Hall::class);
        $concertHall = $this->getDoctrine()->getRepository(ConcertHall::class);

        return $this->render('hall/index.html.twig', [
            "halls" => $halls->findAll(),
            "concertHalls" => $concertHall->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="hall_detail")
     */
    public function detail($id): Response
    {
        $halls = $this->getDoctrine()->getRepository(Hall::class);
        $concertHall = $this->getDoctrine()->getRepository(ConcertHall::class);

        return $this->render('hall/detail.html.twig', [
            "halls" => $halls->findBy(['concertHall' => $concertHall->find($id)]),
            "concertHall" => $concertHall->find($id),
        ]);
    }
}
