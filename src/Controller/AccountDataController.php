<?php

namespace App\Controller;

use App\Form\ChangeDataType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccountDataController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/compte/modifier-infos-perso", name="account_data")
     */
    public function index(Request $request): Response
    {
        $notification = null;

        $user = $this->getUser();
        $form = $this->createForm(ChangeDataType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $firstname = $form->get('firstname')->getData();
            $lastname = $form->get('lastname')->getData();

            $user->setEmail($email);
            $user->setFirstname($firstname);
            $user->setLastname($lastname);

            $this->entityManager->flush();
            $notification = "Vos informations personnelles ont bien été mises à jour";
        }

        return $this->render('account/data.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification
        ]);
    }
}
