<?php

namespace App\Controller;

use App\Entity\Band;
use App\Entity\Show;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted; 

class ConcertController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $show = $this->getDoctrine()->getRepository(Show::class);
        $shows = $show->ComingShow();
        return $this->render('concert/index.html.twig', [
            "shows" => $shows,
            'old' => false,
        ]);
    }

    /**
     * @Route("/detail", name="concert_detail")
     */
    public function detail(): Response
    {
        return $this->render('concert/detail.html.twig');
    }

    /**
     * @Route("/old", name="old_show")
     */
    public function old(): Response
    {
        $show = $this->getDoctrine()->getRepository(Show::class);
        $shows = $show->PassedShow();
        return $this->render('concert/index.html.twig', [
            "shows" => $shows,
            "old" => true,
        ]);
    }
}
