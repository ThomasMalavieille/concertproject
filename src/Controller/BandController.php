<?php

namespace App\Controller;

use App\Entity\Show;
use App\Entity\Band;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted; 

/**
 * @Route("/band")
 */
class BandController extends AbstractController
{
    /**
     * @Route(name="band")
     */
    public function index(): Response
    {
        $bands = $this->getDoctrine()->getRepository(Band::class);

        return $this->render('band/index.html.twig', [
            "bands" => $bands->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="band_detail")
     */
    public function detail($id): Response
    {
        $bands = $this->getDoctrine()->getRepository(Band::class);
        $shows = $this->getDoctrine()->getRepository(Show::class);

        return $this->render('band/detail.html.twig', [
            "shows" =>$shows->ComingShowByBandId($id),
            "band" =>$bands->find($id),
        ]);
    }
}
