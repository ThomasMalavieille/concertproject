<?php

namespace App\Controller\Admin;

use App\Entity\ConcertHall;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ConcertHallCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ConcertHall::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            
            TextField::new('name'),
            IntegerField::new('totalPlaces'),
            TextEditorField::new('presentation'),
            TextField::new('city'),
        ];
    }
}
