<?php

namespace App\Controller\Admin;

use App\Entity\Band;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BandCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Band::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),

            TextField::new('name'),
            AssociationField::new('styles'),
            TextField::new('yearOfCreation'),
            TextField::new('lastAlbumName'),
            ImageField::new('image')->setBasePath('/images')->hideOnForm(),
            TextareaField::new('imageVich')->setFormType(VichImageType::class)->onlyOnForms(),
        ];
    }
}
