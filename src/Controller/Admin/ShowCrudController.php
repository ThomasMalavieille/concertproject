<?php

namespace App\Controller\Admin;

use App\Entity\Show;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ShowCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Show::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),

            DateTimeField::new('date'),
            TextField::new('tourName'),
            AssociationField::new('band'),
            AssociationField::new('hall'),
        ];
    }
}
