<?php

namespace App\Controller\Admin;

use App\Entity\Member;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MemberCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Member::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            
            TextField::new('name'),
            TextField::new('firstName'),
            TextField::new('job'),
            DateField::new('birthDate'),
            AssociationField::new('band'),
            ImageField::new('image')->setBasePath('/images')->hideOnForm(),
            TextareaField::new('imageVich')->setFormType(VichImageType::class)->onlyOnForms(),
        ];
    }
}
