<?php

namespace App\Controller\Admin;

use App\Entity\Band;
use App\Entity\Hall;
use App\Entity\Show;
use App\Entity\Style;
use App\Entity\Member;
use App\Entity\Picture;
use App\Entity\ConcertHall;
use App\Controller\Admin\ShowCrudController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();
        return $this->redirect($routeBuilder->setController(BandCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('ConcertProject');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToUrl('Retour au site', 'fa fa-home', '/public'),
            
            MenuItem::linkToCrud('Groupes', 'fa fa-guitar', Band::class),
            MenuItem::linkToCrud('Membres', 'fa fa-guitar', Member::class),
            MenuItem::linkToCrud('Concerts', 'fa fa-guitar', Show::class),
        ];
    }
}
