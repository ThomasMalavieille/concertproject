<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Member;
use DateTime;
/**
 * Class MemberFixtures
 * @package App\DataFixtures
 */

class MemberFixtures extends Fixture
{

	public const TFK_1 = 'm1';
    public const TFK_2 = 'm2';
    public const TFK_3 = 'm3';

    public const CP_1 = 'm4';
    public const CP_2 = 'm5';
    public const CP_3 = 'm6';
    public const CP_4 = 'm7';

    public const EM = 'm8';
    public const WK = 'm9';
    public const BC = 'm10';


    public function load(ObjectManager $manager)
    {

    	//TFK
        $m1 = new Member();
        $m1->setName('McNevan');
        $m1->setFirstname('Trevor');
        $m1->setJob('Chanteur');
        $m1->setBirthDate(new DateTime('1978-07-17'));
        $m1->setImage('trevor_mcnevan.jpg');
        $manager->persist($m1);

        $m2 = new Member();
        $m2->setName('Augustine');
        $m2->setFirstname('Steve');
        $m2->setJob('Batteur');
        $m2->setBirthDate(new DateTime('1978-07-17'));
        $m2->setImage('steve_augustine.jpg');
        $manager->persist($m2);

        $m3 = new Member();
        $m3->setName('Bruyere');
        $m3->setFirstname('Joel');
        $m3->setJob('Guitariste');
        $m3->setBirthDate(new DateTime('1978-07-17'));
        $m3->setImage('joel_bruyere.jpg');
        $manager->persist($m3);



        //CARAVAN PALACE
        $m4 = new Member();
        $m4->setName('de Bosredon');
        $m4->setFirstname('Arnaud');
        $m4->setJob('Guitariste');
        //$m4->setBirthDate(new DateTime('null'));
        $m4->setImage('arnaud_bosredon.jpg');
        $manager->persist($m4);

        $m5 = new Member();
        $m5->setName('Delaporte');
        $m5->setFirstname('Charles');
        $m5->setJob('Contrebassiste');
        //$m5->setBirthDate(new DateTime('1978-07-17'));
        $m5->setImage('charles_delaporte.jpg');
        $manager->persist($m5);

        $m6 = new Member();
        $m6->setName('Colotis');
        $m6->setFirstname('Zoé');
        $m6->setJob('Chanteuse');
        //$m6->setBirthDate(new DateTime('1978-07-17'));
        $m6->setImage('zoe_colotis.jpg');
        $manager->persist($m6);

        $m7 = new Member();
        $m7->setName('Barbier');
        $m7->setFirstname('Paul-Marie');
        $m7->setJob('Pianiste');
        //$m7->setBirthDate(new DateTime('1978-07-17'));
        $m7->setImage('paulmarie_barbier.jpg');
        $manager->persist($m7);


        //OTHERS
        $m8 = new Member();
        $m8->setName('Eminem');
        $m8->setFirstname('');
        $m8->setJob('Rappeur');
        $m8->setBirthDate(new DateTime('1972-10-17'));
        $m8->setImage('eminem.jpg');
        $manager->persist($m8);

        $m9 = new Member();
        $m9->setName('Makkonen Tesfaye');
        $m9->setFirstname('Abel');
        $m9->setJob('Chanteur');
        $m9->setBirthDate(new DateTime('1990-02-16'));
        $m9->setImage('weeknd.jpg');
        $manager->persist($m9);




        $m10 = new Member();
        $m10->setName('Courtney');
        $m10->setFirstname('Barnaby');
        $m10->setJob('Chanteur');
        $m10->setBirthDate(new DateTime('1990-11-17'));
        $m10->setImage('barns_courtney.jpg');
        $manager->persist($m10);


        $manager->flush();


        $this->addReference(self::TFK_1, $m1);
        $this->addReference(self::TFK_2, $m2);
        $this->addReference(self::TFK_3, $m3);

        $this->addReference(self::CP_1, $m4);
        $this->addReference(self::CP_2, $m5);
        $this->addReference(self::CP_3, $m6);
        $this->addReference(self::CP_4, $m7);

        $this->addReference(self::EM, $m8);
        $this->addReference(self::WK, $m9);
        $this->addReference(self::BC, $m10);
    }
}
