<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Show;
use DateTime;

/**
 * Class ShowFixtures
 * @package App\DataFixtures
 */

class ShowFixtures extends Fixture
{

    public const SHOW_1 = 'sh1';
    public const SHOW_2 = 'sh2';
    public const SHOW_3 = 'sh3';
    public const SHOW_4 = 'sh4';
    public const SHOW_5 = 'sh5';


    public function load(ObjectManager $manager)
    {
        $sh1 = new Show();
        $sh1->setDate(new DateTime('2021-02-07 22:00:00'));
        $sh1->setTourName('Exhale');
        $manager->persist($sh1);

        $sh2 = new Show();
        $sh2->setDate(new DateTime('2021-02-15 20:30:00'));
        $sh2->setTourName('Chronologic');
        $manager->persist($sh2);

        $sh3 = new Show();
        $sh3->setDate(new DateTime('2020-12-01 22:30:00'));
        $sh3->setTourName('Eminem Show');
        $manager->persist($sh3);

        $sh4 = new Show();
        $sh4->setDate(new DateTime('2020-11-28 22:00:00'));
        $sh4->setTourName('After Hours');
        $manager->persist($sh4);

        $sh5 = new Show();
        $sh5->setDate(new DateTime('2021-04-22 20:30:00'));
        $sh5->setTourName('404');
        $manager->persist($sh5);

        $manager->flush();

        $this->addReference(self::SHOW_1, $sh1);
	    $this->addReference(self::SHOW_2, $sh2);
	    $this->addReference(self::SHOW_3, $sh3);
	    $this->addReference(self::SHOW_4, $sh4);
	    $this->addReference(self::SHOW_5, $sh5);
    }

}
