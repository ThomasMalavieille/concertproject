<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\MemberFixtures;
use App\DataFixtures\ShowFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Band;

/**
 * Class BandFixtures
 * @package App\DataFixtures
 */

class BandFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $b1 = new Band();
        $b1->setName('Thousand Foot Krutch');
        $b1->setYearOfCreation('1995');
        $b1->setLastAlbumName('Exhale');
        $b1->setImage('tfk.jpg');
	    $b1->addMember($this->getReference(MemberFixtures::TFK_1));
        $b1->addMember($this->getReference(MemberFixtures::TFK_2));
        $b1->addMember($this->getReference(MemberFixtures::TFK_3));

        $b1->addShow($this->getReference(ShowFixtures::SHOW_1));

        $manager->persist($b1);

        $b2 = new Band();
        $b2->setName('Eminem');
        $b2->setYearOfCreation('1996');
        $b2->setLastAlbumName('Music to Be Murdered By');
        $b2->setImage('eminem.jpg');
        $b2->addMember($this->getReference(MemberFixtures::EM));

        $b2->addShow($this->getReference(ShowFixtures::SHOW_3));

        $manager->persist($b2);


		$b3 = new Band();
        $b3->setName('The Weeknd');
        $b3->setYearOfCreation('2010');
        $b3->setLastAlbumName('After Hours');
        $b3->setImage('weeknd.jpg');
        $b3->addMember($this->getReference(MemberFixtures::WK));

        $b3->addShow($this->getReference(ShowFixtures::SHOW_4));


        $manager->persist($b3);


        $b4 = new Band();
        $b4->setName('Caravan Palace');
        $b4->setYearOfCreation('2008');
        $b4->setLastAlbumName('Chronologic');
        $b4->setImage('caravan_palace.jpg');
        $b4->addMember($this->getReference(MemberFixtures::CP_1));
        $b4->addMember($this->getReference(MemberFixtures::CP_2));
        $b4->addMember($this->getReference(MemberFixtures::CP_3));
        $b4->addMember($this->getReference(MemberFixtures::CP_4));

        $b4->addShow($this->getReference(ShowFixtures::SHOW_2));

        $manager->persist($b4);


        $b5 = new Band();
        $b5->setName('Barns Courtney');
        $b5->setYearOfCreation('2015');
        $b5->setLastAlbumName('404');
        $b5->setImage('barns_courtney.jpg');
        $b5->addMember($this->getReference(MemberFixtures::BC));

        $b5->addShow($this->getReference(ShowFixtures::SHOW_5));

        $manager->persist($b5);


        $manager->flush();
    }


    public function getDependencies()
    {
        return array(
            MemberFixtures::class,
            ShowFixtures::class,
        );
    }

}
