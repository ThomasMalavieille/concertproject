<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;

/**
 * Class UserFixtures
 * @package App\DataFixtures
 */

class UserFixtures extends Fixture
{

	/**
	 * @param ObjectManager $manager
	 */

    public function load(ObjectManager $manager)
    {
        
        $u1 = new User();
        $u1->setEmail('user@user.fr');
        $u1->setRoles([]);
        $u1->setPassword('$argon2id$v=19$m=65536,t=4,p=1$aUpJeW9TZS5ubWpFQm5BNQ$XFnvcCdNqU4kuBHwhleV0GIAqLUXCk9e49XiN9DsbjA');
        $u1->setFirstname('user');
        $u1->setLastname('user');

        $manager->persist($u1);


        $u2 = new User();
        $u2->setEmail('admin@admin.fr');
        $u2->setRoles(['ROLE_ADMIN']);
        $u2->setPassword('$argon2id$v=19$m=65536,t=4,p=1$Wm94WmtudHE2L3VkUWhhRw$lTwjhKIoNsgqbuYa8NsRlaUcSUsG+coIPMbhfmmzgVk');
        $u2->setFirstname('admin');
        $u2->setLastname('admin');

        $manager->persist($u2);

        
        $manager->flush();
    }
}
