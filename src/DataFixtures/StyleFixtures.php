<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Style;

/**
 * Class StyleFixtures
 * @package App\DataFixtures
 */

class StyleFixtures extends Fixture
{

	/**
	 * @param ObjectManager $manager
	 */

    public function load(ObjectManager $manager)
    {
    	$s1 = new Style();
        $s1->setName('Rock');
        $manager->persist($s1);

        $s2 = new Style();
        $s2->setName('Pop');
        $manager->persist($s2);

        $s3 = new Style();
        $s3->setName('Rap');
        $manager->persist($s3);

        $s4 = new Style();
        $s4->setName('R&B');
        $manager->persist($s4);

		$s5 = new Style();
        $s5->setName('Electro');
        $manager->persist($s5);

        $s6 = new Style();
        $s6->setName('Jazz');
        $manager->persist($s6);

        $s7 = new Style();
        $s7->setName('Country');
        $manager->persist($s7);

        $manager->flush();
    }
}
