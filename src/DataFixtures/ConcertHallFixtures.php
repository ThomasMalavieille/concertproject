<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\ConcertHall;

/**
 * Class ConcertHallFixtures
 * @package App\DataFixtures
 */

class ConcertHallFixtures extends Fixture implements DependentFixtureInterface
{

	/**
	 * @param ObjectManager $manager
	 */

    public function load(ObjectManager $manager)
    {
        $ch1 = new ConcertHall();
        $ch1->setName('Paloma');
        $ch1->setTotalPlaces(1500);
        $ch1->setPresentation('Salle de concert à Nîmes crée en 2012');
        $ch1->setCity('Nîmes');
        $ch1->setImage('paloma.jpg');
        $ch1->setAdresse('250, Chemin de l’aérodrome');
        $ch1->addHall($this->getReference(HallFixtures::S1));
        $ch1->addHall($this->getReference(HallFixtures::S2));

        $manager->persist($ch1);

        $ch2 = new ConcertHall();
        $ch2->setName('Zenith Sud');
        $ch2->setTotalPlaces(2000);
        $ch2->setPresentation('Salle de concert de Montpellier sud crée en 1986');
        $ch2->setCity('Montpellier');
        $ch2->setImage('zenith.jpg');
        $ch2->setAdresse('2733, Avenue Albert Einstein');
        $ch2->addHall($this->getReference(HallFixtures::S3));

        $manager->persist($ch2);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            HallFixtures::class,
        );
    }
}
