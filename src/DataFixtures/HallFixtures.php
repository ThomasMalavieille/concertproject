<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\ShowFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Hall;

/**
 * Class HallFixtures
 * @package App\DataFixtures
 */

class HallFixtures extends Fixture implements DependentFixtureInterface
{

	public const S1 = 'h1';
    public const S2 = 'h2';
    public const S3 = 'h3';

    public function load(ObjectManager $manager)
    {
        $h1 = new Hall();
        $h1->setName('Salle 1');
        $h1->setCapacity(750);
        $h1->setAvailable(1);
        $h1->addShow($this->getReference(ShowFixtures::SHOW_2));
        $manager->persist($h1);

        $h2 = new Hall();
        $h2->setName('Salle 2');
        $h2->setCapacity(750);
        $h2->setAvailable(0);
        $h2->addShow($this->getReference(ShowFixtures::SHOW_3));
        $manager->persist($h2);

        $h3 = new Hall();
        $h3->setName('Salle 1');
        $h3->setCapacity(2500);
        $h3->setAvailable(1);
        $h3->addShow($this->getReference(ShowFixtures::SHOW_1));
        $h3->addShow($this->getReference(ShowFixtures::SHOW_4));
        $h3->addShow($this->getReference(ShowFixtures::SHOW_5));
        $manager->persist($h3);

        $manager->flush();

        $this->addReference(self::S1, $h1);
        $this->addReference(self::S2, $h2);
        $this->addReference(self::S3, $h3);
    }

    public function getDependencies()
    {
        return array(
            ShowFixtures::class,
        );
    }
}
